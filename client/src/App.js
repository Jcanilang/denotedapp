import React from 'react';
import './App.css';
import "react-bulma-components/dist/react-bulma-components.min.css";
import { BrowserRouter, Switch, Route} from "react-router-dom";
import ApolloClient from "apollo-boost";
import {ApolloProvider} from "react-apollo";

import Todo from "./components/Todo";
import NavBar from "./components/NavBar";
import UpdateTitle from "./components/UpdateTitle";
import UpdateNote from "./components/UpdateNote";

 const client = new ApolloClient({ uri:"http://localhost:8000/api"});

function App() {


  return (
     <ApolloProvider client={client}>
      <BrowserRouter>
      <NavBar/>
        <Switch>
          <Route exact path="/" component={Todo} />
          <Route exact path="/update/title/:id" component={UpdateTitle}/>
          <Route exact path="/update/note/:id" component={UpdateNote}/>
        </Switch>
      </BrowserRouter>
    </ApolloProvider>

  );
}

export default App;

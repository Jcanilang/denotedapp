import React,{ useState , useEffect, setState} from "react";
import "../App.css";
import { Section, Heading, Button, Container, Columns, Card } from "react-bulma-components";
import {Link, Redirect} from "react-router-dom";
import {flowRight as compose} from "lodash"
import { graphql } from "react-apollo";


import {getNoteQuery,getTitlesQuery} from "../queries/queries";
import {updateNoteMutation} from "../queries/mutations"

const UpdateNote = props =>{

	const dataUpdateNote = props.updateNoteMutation;
	const dataTitles = props.getTitlesQuery;

	const titleData = dataTitles.getTitles ? dataTitles.getTitles :[];
	let noteData = props.getNoteQuery.getNote? props.getNoteQuery.getNote :{};

	const [note, setNote] = useState("");
	const [titleId, setTitleId] = useState("");
	const [id, setId] = useState("");


	if(!props.getNoteQuery.loading){

		const setDefaultNote = ()=>{
			setNote(noteData.note);
			setTitleId(noteData.titleId);
			setId(noteData.id);
		}
		if(id === ""){
			setDefaultNote();
		}

	}

	const noteChangeHandler = e =>{
		// console.log(e.target.value)
		setNote(e.target.value);

	}

	const titleIdChangeHandler = e => {
		// console.log(e.target.value)
		setTitleId(e.target.value);
	}

	const formSubmitHandler = e => {
		e.preventDefault();

		let updateNote = {
			id: noteData.id,
			note: note,
			titleId:titleId
		}

		dataUpdateNote({
			variables:updateNote
			});

		window.location.href="/";
	}

	return (
		
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card>
						<Card.Header>
							<Card.Header.Title>Note Details</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={formSubmitHandler}>
							<div className="field">
									<label className="label" htmlFor="titleId">
										Title
									</label>
									<div className="control">
										<div className="select is-fullwidth">
											<select id="titleId" onChange={titleIdChangeHandler}>
													<option disabled selected > -- Select Title -- </option>
													{
														titleData.map(title=>{
														return(
														<option key={title.id} value={title.id} selected={title.id === titleId? true:false}>
														 {title.title}
														</option>
														);
													})
													}
											</select>
										</div>
									</div>
								</div>

								<div className="field">
									<label
										className="label"
										htmlFor="Note"
									>
										Note
									</label>
									<input
										className="input"
										type="text"
										id="note"
										onChange ={noteChangeHandler}
										value={note}
									/>
								</div>
								<Button type="submit" color="success" fullwidth>
									Update Note
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
		)
}

export default compose(
	graphql(getTitlesQuery,{name:"getTitlesQuery"}),
	graphql(updateNoteMutation,{name:"updateNoteMutation"}),
	graphql(getNoteQuery,{
		name:"getNoteQuery",
		//retrieve the wild card
		options: props => {
				return{ 
					variables: { 
						id: props.match.params.id 
					} 
				}
			}
	}),
	)(UpdateNote);

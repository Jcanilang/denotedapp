import React,{useState} from "react";
import "../App.css";
import { Button, Container, Columns,Card } from "react-bulma-components";
import { graphql } from "react-apollo";
import { Link } from "react-router-dom";

import {flowRight as compose} from "lodash"
import { 
	getTitlesQuery,
	getTitleQuery,
	getNotesQuery,
	getNoteQuery 
} from "../queries/queries"; 

import { 
  createTitleMutation,
  createNoteMutation,
  deleteTitleMutation,
  deleteNoteMutation
} from "../queries/mutations"; 
//body components
const Todo = props => {

	const dataTitles = props.getTitlesQuery;
	const dataNotes = props.getNotesQuery;

	const titleData = dataTitles.getTitles ? dataTitles.getTitles :[];
	const noteData = dataNotes.getNotes ? dataNotes.getNotes :[];
		
	const [title, setTitle] = useState("");
	const [titleId, setTitleId] = useState("");
	const [note, setNote] = useState("");
	// const [isCompleted, setIsCompleted] = useState("");

	// console.log(titleData)
	const addTitle = e =>{
		e.preventDefault();

		let newTitle = {
			title: title
		}

		props.createTitleMutation({
			variables:newTitle,
			refetchQueries:[{
				query:getTitlesQuery
			}]
		})

		setTitle("");
		
	}

	const addNote = e =>{
		e.preventDefault();

		let newNote = {
			titleId: titleId,
			note:note
		}

		props.createNoteMutation({
			variables:newNote,
			refetchQueries:[{
				query:getTitlesQuery
			}]
		})

		setTitleId("");
		setNote("");
		
	}

	const titleChangeHandler = e =>{
		// console.log(e.target.value)
		setTitle(e.target.value);

	}

	const titleIdChangeHandler = e => {
		// console.log(e.target.value)
		setTitleId(e.target.value);
	}

	const noteChangeHandler = e => {
		// console.log(e.target.value)
		setNote(e.target.value);
	}



	const deleteTitleHandler= e =>{
		let id = e.target.id;
        // console.log(id);
              props.deleteTitleMutation({
                    variables: {id:id},
                    refetchQueries:[{
                        query:getTitlesQuery
                        }]
                });
              // console.log(id);
	}

	const deleteNoteHandler= e =>{
		let id = e.target.id;
        // console.log(id);
              props.deleteNoteMutation({
                    variables: {id:id},
                    refetchQueries:[{
                        query:getTitlesQuery
                        }]
                });
              // console.log(id);
	}




	return (
		<Container>
			<Columns>
				<Columns.Column size={3}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Add Title</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={addTitle}>
								<div className="field">
									<label
										className="label"
										htmlFor="title"
									>
										Title
									</label>
									<input
										className="input"
										type="text"
										id="title"
										onChange ={titleChangeHandler}
										value={title}
									/>
								</div>
								<Button type="submit" color="success" fullwidth>
									Add Tile
								</Button>
							</form>
						</Card.Content>
					</Card>

					<hr/>

					<Card>
						<Card.Header>
							<Card.Header.Title>Add Note</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={addNote}>
								
								<div className="field">
									<label className="label" htmlFor="titleId">
										Title
									</label>
									<div className="control">
										<div className="select is-fullwidth">
											<select id="titleId" onChange={titleIdChangeHandler}>
													<option disabled selected > -- Select Title -- </option>
													{
														titleData.map(title=>{
														return(
														<option key={title.id} value={title.id}>
														 {title.title}
														</option>
														);
													})
													}
											</select>
										</div>
									</div>
								</div>

								<div className="field">
									<label
										className="label"
										htmlFor="note"
									>
										Note
									</label>
									<input
										className="input"
										type="text"
										id="note"
										onChange ={noteChangeHandler}
										value={note}
									/>
								</div>
								<Button type="submit" color="success" fullwidth>
									Add Note
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>

				<Columns.Column size={9}>
					<Card>
						<Card.Header>
									<Card.Header.Title>To Do List</Card.Header.Title>
								</Card.Header>
								<Card.Content>
									<ul>
						    {titleData.map(title=>{
						    	return(
						    		<li>
						    	<Card>
						    	

						    		<Card.Content>

						    			<input value={title.id} type="checkbox" checked={title.isCompleted}/> {title.title} 
						    			<div className="field has-addons is-pulled-right">
										  <p className="control">
										  <Link to={"/update/title/"+title.id}>
										    <button className="button is-info is-small">
										      Update
										    </button>
										   </Link>

										  </p>
										  <p className="control">
										    <button className="button is-danger is-small" id={title.id} onClick={deleteTitleHandler}>
										      Delete
										    </button>
										  </p>
										</div>
										<div className="content">
						    			<ul type="circle">
						    				{title.notes.map(note=>{
						    					return(
						    						<li>
						    							{note.note} 
										    			<div className="field has-addons is-pulled-center">
														  <p className="control">
														    <Link to={"/update/note/"+note.id}>
														    <button className="button is-info is-small">
														      Update
														    </button>
														   </Link>
														  </p>
														  <p className="control">
														    <button className="button is-danger is-small" id={note.id} onClick={deleteNoteHandler}>
														      Delete
														    </button>
														  </p>
														</div>
						    						</li>
						    						);
						    					}
						    				)}
						    			</ul>
						    			</div>
						    		</Card.Content>
						    	</Card>
						    </li>
						    	);
						    })}
						    
						    </ul>

						</Card.Content>
					</Card>	    
				</Columns.Column>
			</Columns>
		</Container>
	);
};



export default compose(
	graphql(getTitlesQuery,{name:"getTitlesQuery"}),
	graphql(getNotesQuery,{name:"getNotesQuery"}),
	graphql(createTitleMutation,{name:"createTitleMutation"}),
  	graphql(createNoteMutation,{name:"createNoteMutation"}),
  	graphql(deleteTitleMutation,{name:"deleteTitleMutation"}),
 	graphql(deleteNoteMutation,{name:"deleteNoteMutation"}),
 	graphql(getNoteQuery,{name:"getNoteQuery"}),
 	graphql(getTitleQuery,{name:"getTitleQuery"}),
 	)(Todo);

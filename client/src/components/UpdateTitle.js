import React,{ useState , useEffect, setState} from "react";
import "../App.css";
import { Section, Heading, Button, Container, Columns, Card } from "react-bulma-components";
import {Link, Redirect} from "react-router-dom";
import {flowRight as compose} from "lodash"
import { graphql } from "react-apollo";


import {getTitleQuery} from "../queries/queries";
import {updateTitleMutation} from "../queries/mutations"

const UpdateTitle = props =>{

	const dataUpdateTitle = props.updateTitleMutation;

	let titleData = props.getTitleQuery.getTitle? props.getTitleQuery.getTitle :{};

	const [title, setTitle] = useState("");
	const [isCompleted, setIsCompleted] = useState(false);
	const [id, setId] = useState("");

	// console.log(props)
	if(!props.getTitleQuery.loading){

		const setDefaultTitle = ()=>{
			setTitle(titleData.title);
			setIsCompleted(titleData.isCompleted);
			setId(titleData.id);
		}
		if(id === ""){
			setDefaultTitle();
		}

	}

	

	const titleChangeHandler = e =>{
		// console.log(e.target.value)
		setTitle(e.target.value);

	}

	const isCompletedChangeHandler = e =>{
		// if(e.target.checked){
		// setIsCompleted(true);
		// }
		// else{
		setIsCompleted(e.target.checked);
		// console.log(e.target.checked)
		// }

		
		
	}

	const formSubmitHandler = e => {
		e.preventDefault();
			// console.log(isCompleted)
		let updateTitle = {
			id: titleData.id,
			title: title,
			isCompleted:isCompleted
		}

		dataUpdateTitle({
			variables:updateTitle
			});

		window.location.href="/";
	}

	return (
		
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card>
						<Card.Header>
							<Card.Header.Title>Title Details</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={formSubmitHandler}>
								<div className="field">
									<label
										className="label"
										htmlFor="title"
									>
										Title
									</label>
									<input
										className="input"
										type="text"
										id="title"
										onChange ={titleChangeHandler}
										value={title}
									/>
								</div>
								<input type="checkbox" onChange={isCompletedChangeHandler} checked={isCompleted} /> Complete?
								<div className="field">
								<Button type="submit" size="small" fullwidth color="success">
									Update Title
								</Button>
								<Link to="/">
								<Button type="button" size="small" fullwidth color="danger">
									Cancel
								</Button>
								</Link>
								</div>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
		)
}

export default compose(
	graphql(updateTitleMutation,{name:"updateTitleMutation"}),
	graphql(getTitleQuery,{
		name:"getTitleQuery",
		//retrieve the wild card
		options: props => {
				return{ 
					variables: { 
						id: props.match.params.id 
					} 
				}
			}
	}),
	)(UpdateTitle);

import React, { useState } from "react";
import { Navbar } from "react-bulma-components";
import { Link } from "react-router-dom";
// header components
const NavBar = props => {
	// console.log(props.username);


	return (
		<Navbar color="black" fixed={"top"}>
			<Navbar.Brand>
				<Link to="/" className="navbar-item">
					<strong>To Do App </strong>  || J-Rex Hero Canilang
				</Link>

			</Navbar.Brand>
		</Navbar>
	);
};

export default NavBar;

import {gql} from "apollo-boost";



const createTitleMutation = gql `
mutation(
$title:String!
){
  createTitle(
  title:$title
  ){
    id
    title
  }
}`;

const createNoteMutation =  gql `

mutation(
	$titleId:String!
	$note:String!
	){
  createNote(
    titleId:$titleId
    note:$note
  ){
    titleId
    note
  }
}`;


const deleteTitleMutation = gql`
  mutation($id:String!){
    deleteTitle(id:$id)
  }
`;

const deleteNoteMutation = gql`
  mutation($id:String!){
    deleteNote(id:$id)
  }
`;



const updateTitleMutation = gql`
  mutation(
  $id:ID! 
  $title:String!
  $isCompleted:Boolean
  ){
    updateTitle(
    id:$id 
    title:$title
    isCompleted:$isCompleted
    ){
      id
      title
      isCompleted
    }
  }
`;

const updateNoteMutation = gql`
 mutation(
  $id:ID! 
  $titleId:String! 
  $note:String! 
  ){
    updateNote(
    id:$id 
    titleId:$titleId 
    note:$note
    ){
      id
      titleId
      note
    }
  }
`;


export {

  createTitleMutation,
  createNoteMutation,

  deleteTitleMutation,
  deleteNoteMutation,

  updateTitleMutation,
  updateNoteMutation

}

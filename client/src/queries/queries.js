
// need to intall packages
// npm i apollo-boost react-apollo graphql-tag graphql

import {gql} from "apollo-boost";

const getTitlesQuery = gql`
{
  getTitles{
    id
    title
    isCompleted
    notes{
      id
      note
    }
  }
}
`;


const getTitleQuery = gql `
query(
$id:ID!
){
  getTitle(id:$id){
    id
    title
    isCompleted
    notes{
    	id
      note
    }
  }
} 
`;

const getNotesQuery = gql`
{
  getNotes{
    id
    titleId
    note
    title{
      title
    }
  }
}
`;

const getNoteQuery = gql`
query(
$id:ID!
){
  getNote(id:$id){
    id
    titleId
    note
    title{
      title
    }
  }
}
`;

export {getTitlesQuery,getTitleQuery,getNotesQuery,getNoteQuery}

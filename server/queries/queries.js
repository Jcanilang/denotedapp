const {ApolloServer,gql} = require("apollo-server-express");
const mongoose = require("mongoose");
const Note = require("../models/Note.js");
const Title = require("../models/Title.js");

const typeDefs = gql`

	type NoteType{
		id:ID
		titleId:String!
		note:String!
		isDeleted:Boolean!
		title: TitleType
	}
	type TitleType{
		id:ID
		title:String!
		isCompleted:Boolean!
		isDeleted:Boolean!
		notes:[NoteType]
	}

	type Query{
		getNotes:[NoteType]
		getNote(id:ID!) :NoteType

		getTitles:[TitleType]
		getTitle(id:ID!) :TitleType
	}

	type Mutation{

		createNote(
		titleId:String!
		note:String!
		):NoteType

		createTitle(
		title:String!
		):TitleType

		updateNote(
		id:ID!
		titleId:String!
		note:String!
		):NoteType

		updateTitle(
		id:ID!
		title:String!
		isCompleted:Boolean
		):TitleType

		deleteNote(
		id:String!
		):Boolean

		deleteTitle(
		id:String!
		):Boolean

	}



`;

const resolvers = {
	Query:{
		getNotes:()=>{
			return Note.find({isDeleted:false})
		},
		getNote:(_,args)=>{
			return Note.findById(args.id)
		},
		getTitles:()=>{
			return Title.find({isDeleted:false})
		},
		getTitle:(_,args)=>{
			return Title.findById(args.id)
		}

	},

	Mutation: {
		
		createNote:(_,args)=>{
			let newNote = Note({
				titleId:args.titleId,
				note:args.note,
				isCompleted:false,
				isDeleted:false
			})
			return newNote.save()
		},

		createTitle:(_,args)=>{
			let newTitle = Title({
				title:args.title,
				isCompleted:false,
				isDeleted:false
			})
			return newTitle.save()
		},

		updateNote:(_,args)=>{
			let condition = {_id:args.id}
			let update = {
				titleId:args.titleId,
				note:args.note,
				isDeleted:false	
			}
			return Note.findOneAndUpdate(condition,update)
		},
		updateTitle:(_,args)=>{
			let condition = {_id:args.id}
			let update = {
				title:args.title,
				isCompleted:args.isCompleted,
				isDeleted:false	
			}
			return Title.findOneAndUpdate(condition,update)
		},
		deleteNote:(_,args)=>{
			let id = mongoose.Types.ObjectId(args.id);
			let condition = {_id:id}
			let update={
				isDeleted:true
			}
			return Note.findOneAndUpdate(condition,update).then((note,err)=>{
				if(err ||!note){
					console.log("Delete failed");
					return false;
				}else{
					console.log("Note Deleted")
					return true;
				}

			})
		},
		deleteTitle:(_,args)=>{
			let id = mongoose.Types.ObjectId(args.id);
			let condition = {_id:id}
			let update={
				isDeleted:true
			}
			return Title.findOneAndUpdate(condition,update).then((title,err)=>{
				if(err ||!title){
					console.log("Delete failed");
					return false;
				}else{
					console.log("Title Deleted")
					return true;
				}

			})
		}

	},
	NoteType:{
		title:(parent,args)=>{
			return Title.findOne({_id:parent.titleId})
		}
	},
	TitleType:{
		notes:(parent,args)=>{
			return Note.find({titleId:parent.id}).find({isDeleted:false})
		}
	},
	
}

const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports =server;
const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 8000;

app.get('/',function(req,res){
	res.send('Welcome to ToDo App Server by J-Rex Hero Canilang');
});

// online
const connectionStr = "mongodb+srv://user:0ptional@firstcluster-iz6sr.mongodb.net/tododb?retryWrites=true&w=majority";



mongoose.connect(connectionStr,{useNewUrlParser:true});

mongoose.connection.once('open',()=>{
	console.log('now connected to the mongodb server')
});


const server = require("./queries/queries.js");


server.applyMiddleware({app,path:"/api"});



//server initialization
app.listen(port,()=>{
	console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`);

});


const mongoose = require("mongoose")
const Schema = mongoose.Schema

const noteSchema = new Schema({
	titleId:{
		type:String,
		required:true
	},
	note:{
		type:String,
		required:true
	},
	isDeleted:{
		type:Boolean
	}
},{
	timestamps:true
})

 // export the model as a module
 module.exports = mongoose.model('Note', noteSchema);
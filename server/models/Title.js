const mongoose = require("mongoose")
const Schema = mongoose.Schema

const titleSchema = new Schema({
	title:{
		type:String,
		required:true
	},
	isCompleted:{
		type:Boolean
	},
	isDeleted:{
		type:Boolean
	}
},{
	timestamps:true
})

 // export the model as a module
 module.exports = mongoose.model('Title', titleSchema);